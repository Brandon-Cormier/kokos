﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class CameraScript : MonoBehaviour
{
    private Kokonut kokonut;
    private string PlayerName = "";
    private bool Invert = false;
    public Transform player;
    private float distance = 6.0f;

    private float height = 2.5f;

    private float xSpeed = 90;
    private float ySpeed = 90;

    private float yMin = -40;
    private float yMax = 70;

    private float x = 0.0f;
    private float y = 0.0f;

    void Start()
    {
        transform.rotation = Quaternion.identity;
        Vector3 angles = transform.eulerAngles;
        x = angles.x;
        y = angles.y;
        kokonut = transform.parent.gameObject.GetComponent<Kokonut>();

        foreach (KeyValuePair<int, string> entry in GameSettings.ControllerSet)
        {
            if (entry.Value == kokonut.GetID())
            {
                PlayerName = "P" + entry.Key.ToString();
            }
        }

        GetInvert();
    }

    private string PlayerControl(string control)
    {
        if (PlayerName == "")
        {
            PlayerName = "P0";
        }
        return PlayerName + "_" + control;
    }

    private void GetInvert()
    {
        if (GameSettings.Invert != null)
        {
            if (PlayerName == "")
            {
                PlayerName = "P0";
            }
            Invert = GameSettings.Invert[PlayerName];
        }
    }

    void LateUpdate()
    {
        GetInvert();

        if (player)
        {
            x += Input.GetAxisRaw(PlayerControl("RightX")) * xSpeed * 0.02f;

            if (Input.GetAxis(PlayerControl("RightX")) > -0.3 && Input.GetAxis(PlayerControl("RightX")) < 0.3)
            {
                kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnLeft", false);
                kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnRight", false);
                kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("NoTurn", true);
            }
            else
            {
                kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("NoTurn", false);
                if (Input.GetAxis(PlayerControl("RightX")) < -0.2)
                {
                    kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnRight", false);
                    kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnLeft", true);
                }
                else if (Input.GetAxis(PlayerControl("RightX")) > 0.2)
                {
                    kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnLeft", false);
                    kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnRight", true);
                }
            }

            if (Invert)
            {
                y += Input.GetAxisRaw(PlayerControl("RightY_Inv")) * ySpeed * 0.02f;
            }
            else
            {
                y += Input.GetAxisRaw(PlayerControl("RightY")) * ySpeed * 0.02f;
            }

            y = ClampAngle(y, yMin, yMax);

            Quaternion rotation = Quaternion.Euler(y, x, 0);
            Vector3 position = rotation * (new Vector3(0.0f, height, -distance)) + player.position;
            transform.rotation = rotation;
            transform.position = position;
        }
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }

    public void StopCamera()
    {
        transform.rotation = Quaternion.Euler(y, x, 0);
    }
}

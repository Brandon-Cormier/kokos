﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DestroyByTime : MonoBehaviour {

	public float lifetime;
    private Interactable Throwable;

    private bool CanDelete = false;
    private float DeleteTimer = 0f;

	void Start()
	{
        Throwable = GetComponent<Interactable>();
	}

    private void Update()
    {
        if (CanDelete)
        {
            if (Throwable.attachedToHand == null)
            {
                DeleteTimer += Time.deltaTime;
                if (DeleteTimer >= lifetime)
                {
                    Destroy(gameObject);
                    DeleteTimer = 0f;
                }
            }
            else
            {
                DeleteTimer = 0f;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Respawn")
        {
            CanDelete = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Respawn")
        {
            CanDelete = false;
        }
    }
}

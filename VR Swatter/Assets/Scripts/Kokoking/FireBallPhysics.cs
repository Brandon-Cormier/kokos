﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FireBallPhysics : MonoBehaviour
{
    public GameObject Display;
    private int FireBallPoints = 0;
    public float SmackPower;
    private Rigidbody rb;
    private List<GameObject> _activePlayers;

    // Use this for initialization
    void Start()
    {
        Physics.IgnoreLayerCollision(gameObject.layer, 16, true);
        Physics.IgnoreLayerCollision(gameObject.layer, gameObject.layer, true);
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 2);
    }

    public void SetScore(int score)
    {
        FireBallPoints = score;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Terrain" || collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Collider")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Kokonut")
        {
            if (collision.relativeVelocity.magnitude > 10)
            {
                collision.rigidbody.AddForce(rb.velocity * SmackPower);
                Kokonut kokonut = collision.gameObject.GetComponent<Kokonut>();
                if (!kokonut.IsSmacked())
                {
                    kokonut.GotSmacked();
                    kokonut.StartDisabledTimer();
                    GameObject ScoreDisplay = Instantiate(Display, kokonut.transform.position, Quaternion.identity);
                    Kokonut[] inLead = GetWinningKokonuts();
                    if (inLead.Contains(kokonut) && kokonut.GetNumFeathers() > 0)
                    {
                        if (inLead.Length == 1)
                        {
                            FireBallPoints += FireBallPoints / 2;
                        }
                        else
                        {
                            FireBallPoints += FireBallPoints / 4;
                        }
                    }
                    ScoreDisplay.transform.Find("Canvas/Score").gameObject.GetComponent<Text>().text = "+" + FireBallPoints.ToString();
                    ScoreDisplay.GetComponent<Animator>().SetTrigger("ScoreDisplay");
                    ScoreManager.UpdateScore(FireBallPoints);
                }
            }
        }
    }

    private Kokonut[] GetWinningKokonuts()
    {
        _activePlayers = GameObject.FindGameObjectsWithTag("Kokonut").Where(player => player.activeInHierarchy).ToList();
        try
        {
            var topScore = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Select(kokonut => kokonut.GetNumFeathers())
                .Max();
            var winners = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Where(kokonut => kokonut.GetNumFeathers() == topScore)
                .ToArray();
            return winners;
        }
        catch
        {
            return new Kokonut[] { };
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGustPhysics : MonoBehaviour
{
    public int layer;
    public float SmackPower;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        gameObject.layer = layer;
        Physics.IgnoreLayerCollision(layer, layer, true);
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 0.5f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Kokonut")
        {
            if (gameObject.layer != collision.gameObject.layer)
            {                
                collision.rigidbody.AddForce(rb.velocity * SmackPower);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class Smack : MonoBehaviour {

    public GameObject Display;
    private int SmackPoints = 200;
    private int DefaultSmackPoints = 200;
    public float SmackPower;
    private Hand hand;
    private List<GameObject> _activePlayers;

    // Use this for initialization
    void Start () {
        hand = GetComponent<Hand>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Throwable")
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }

        if (collision.gameObject.tag == "Kokonut")
        {
            if (collision.relativeVelocity.magnitude > 10)
            {
                collision.rigidbody.AddForce(hand.GetTrackedObjectVelocity() * SmackPower);
                Kokonut kokonut = collision.gameObject.GetComponent<Kokonut>();
                if (!kokonut.IsSmacked())
                {
                    kokonut.GotSmacked();
                    kokonut.StartDisabledTimer();
                    GameObject ScoreDisplay = Instantiate(Display, kokonut.transform.position, Quaternion.identity);
                    Kokonut[] inLead = GetWinningKokonuts();
                    if (inLead.Contains(kokonut) && kokonut.GetNumFeathers() > 0)
                    {
                        if (inLead.Length == 1)
                        {
                            SmackPoints += SmackPoints / 2;
                        }
                        else
                        {
                            SmackPoints += SmackPoints / 4;
                        }
                    }
                    ScoreDisplay.transform.Find("Canvas/Score").gameObject.GetComponent<Text>().text = "+" + SmackPoints.ToString();
                    ScoreDisplay.GetComponent<Animator>().SetTrigger("ScoreDisplay");
                    ScoreManager.UpdateScore(SmackPoints);
                    SmackPoints = DefaultSmackPoints;
                }
            }
        }
    }

    private Kokonut[] GetWinningKokonuts()
    {
        _activePlayers = GameObject.FindGameObjectsWithTag("Kokonut").Where(player => player.activeInHierarchy).ToList();
        try
        {
            var topScore = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Select(kokonut => kokonut.GetNumFeathers())
                .Max();
            var winners = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Where(kokonut => kokonut.GetNumFeathers() == topScore)
                .ToArray();
            return winners;
        }
        catch
        {
            return new Kokonut[] { };
        }
    }
}

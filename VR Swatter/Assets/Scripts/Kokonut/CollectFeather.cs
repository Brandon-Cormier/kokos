using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class CollectFeather : MonoBehaviour
{
    public float CheckpointSoundPitch;
    public List<AudioSource> FeatherCollectionSound;
    public AudioSource FeatherCheckpointSound;
    private Kokonut kokonut;
    public GameObject featherCollectImg;

    private float respawncounter = 0f;
    public float respawntime = 2;
    private int collected = 0;
    private int[] x_range = { -50, 130 };
    private int[] y_range = { 20, 75 };
    private int[] z_range = { 100, 185 };

    private Queue<GameObject> feathersToRespawn;

    private System.Random rnd = new System.Random();

    void Start()
    {
        SetPitch(CheckpointSoundPitch);
        kokonut = GetComponent<Kokonut>();
        feathersToRespawn = new Queue<GameObject>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Feather")
        {
            if (!kokonut.IsSmacked())
            {
                other.gameObject.SetActive(false);
                GameObject fImg = Instantiate(featherCollectImg);
                fImg.transform.SetParent(kokonut.transform.Find(kokonut.GetID() + " Camera/Canvas/FImgSpawn").transform);
                fImg.transform.rotation = new Quaternion(0, 0, 0, 0);
                kokonut.CollectFeathers(1);
                var feathers = kokonut.GetNumFeathers();
                // TODO: remove this constant. Logic should be based
                // on total number of feathers to win 
                if (feathers > 0 && feathers % 5 == 0)
                {
                    transform.Find("KOKO").GetComponent<Animator>().SetTrigger("FeatherCollect");
                    transform.Find("KOKO").GetComponent<Animator>().SetTrigger("Idle");
                    kokonut.PlayLaugh();
                }
                else
                {
                    SetPitch(CheckpointSoundPitch);
                    if (FeatherCheckpointSound)
                    {
                        FeatherCheckpointSound.Play();
                    }
                }

                // if we want to fade out the destroy, we can call a method
                // that continues to lower the alpha channel, and then
                // destroying when alpha <= 0.
                feathersToRespawn.Enqueue(other.gameObject);
                collected++;
                rnd = new System.Random(System.DateTime.Now.Millisecond);
                int positionx = rnd.Next(x_range[0], x_range[1]);
                int positiony = rnd.Next(y_range[0], y_range[1]);
                int positionz = rnd.Next(z_range[0], z_range[1]);

                other.transform.SetPositionAndRotation(new Vector3((float)positionx, (float)positiony, (float)positionz), other.transform.rotation);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (collected > 0)
        {
            respawncounter += Time.deltaTime;
            if (respawncounter >= respawntime && Mathf.FloorToInt(respawncounter % respawntime) == 0)
            {
                GameObject nextFeather = feathersToRespawn.Dequeue();
                nextFeather.SetActive(true);
                respawncounter = 0f;
                collected--;
            }
        }
    }

    private void SetPitch(float pitch)
    {
        if (FeatherCheckpointSound)
        {
            FeatherCheckpointSound.pitch = CheckpointSoundPitch;
        }
    }
}

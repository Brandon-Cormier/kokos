﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGustSpawn : MonoBehaviour {

    public GameObject WindEffect;
    public GameObject WindAttack;
    public Transform Spawnpoint;
    public float speed;

    private bool Shooting = false;
    private float fireTimer = 0f;
    private float fireDelay = 0.1f;

    private float firstShotTimer = 0f;
    private float firstShotDelay = 0.5f;

    private bool firstFired = false;
    private Vector3 velocity;

    private string PlayerName = "";
    private Kokonut koko;

    // Use this for initialization
    void Start()
    {
        koko = GetComponent<Kokonut>();

        foreach (KeyValuePair<int, string> entry in GameSettings.ControllerSet)
        {
            if (entry.Value == koko.GetID())
            {
                PlayerName = "P" + entry.Key.ToString();
            }
        }
    }

    private string PlayerControl(string control)
    {
        if (PlayerName == "")
        {
            PlayerName = "P0";
        }
        return PlayerName + "_" + control;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstFired)
        {
            firstShotTimer += Time.deltaTime;
        }

        if (firstShotTimer >= firstShotDelay)
        {
            firstFired = false;
            firstShotTimer = 0f;
        }

        if (Input.GetButtonDown(PlayerControl("R1")))
        {
            Shooting = true;
            Spawnpoint.transform.Find("WindEffect").gameObject.SetActive(true);
            if (!firstFired)
            {
                Shoot();
                firstFired = true;
                firstShotTimer = 0f;
            }
        }
        if (Input.GetButtonUp(PlayerControl("R1")))
        {
            Spawnpoint.transform.Find("WindEffect").gameObject.SetActive(false);
            Shooting = false;
            fireTimer = 0f;
        }

        if (Shooting)
        {
            if (fireTimer < fireDelay)
            {
                fireTimer += Time.deltaTime;
            }
            if (fireTimer >= fireDelay)
            {
                Shoot();
                fireTimer = 0f;
                firstShotTimer = 0f;
            }
        }

    }
    void Shoot()
    {
        GameObject windAttack = Instantiate(WindAttack, Spawnpoint.position, Spawnpoint.rotation);
        Rigidbody rb = windAttack.GetComponent<Rigidbody>();
        windAttack.layer = gameObject.layer;
        rb.velocity = Spawnpoint.forward * speed;
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    enum GameState { PreGame, PreRound, Round, Overtime, Paused, /*BuffSelection  , PostGame */ PostRound };
    public GameObject Feather;

    public GameObject[] players;
    public GameObject KokoKing;
    public Animator GameUIAnimator;
    public Animator VRUIAnimator;
    public Animator VRTimerCanvas;
    public GameObject TimerRHand;
    public Text VRStartTimerText;
    public Text VRGameOverText;
    public Text StartGameText;
    public Text GameOverText;
    public Text OvertimeText;
    public Material _yellowMat;
    public Material _grayMat;
    public GameObject RockSpawn1;
    public GameObject RockSpawn2;
    public GameObject leftHand;
    public GameObject rightHand;
    public AudioSource LevelMusic;
    public AudioSource LevelMusicFast;

    public GameObject PreGameCutScene;
    public GameObject Keyboard;
    public GameObject[] Leaderboards;

    [Header("Helpers")]
    [Tooltip("The number of seconds before showing a helper again")]
    public float SecondsBetweenHelperRepeat = 20f;
    public float ShowJumpHelperAfterSeconds = 10f;
    [Tooltip("The number of times a player needs to jump to stop jump helper from showing")]
    public int StopJumpHelperAfterJumpCount = 5;
    public float ShowDiveHelperAfterSeconds = 30f;
    [Tooltip("The number of times a player needs to dive to stop dive helper from showing")]
    public int StopDiveHelperAfterDiveCount = 3;

    private bool _jumpHelperShown = false;
    private bool _diveHelperShown = false;

    private int _numFeathersToWin;

    private int _secondsInRound;
    private float _levelMusicVolume;
    private float _levelMusicFastVolume;
    float _cutSceneTimer = 0f;

    private float _timeLeftInRound;
    private float _startGameTimer = 4f;
    private float _startGameDelay = 0f;

    private int _numPlayers;
    private Kokonut _winningKokonut;
    private bool KokoKingWin = false;
    private int KokoKingScore;
    private float showKeyboardTimer = 0f;
    private float showKeyboardDelay = 5f;
    private float showLeaderboardTimer = 0f;
    private float showLeaderboardDelay = 7f;
    private bool _DoneShowingKeyboard = false;
    private bool _DoneShowingLeaderboard = false;

    //private Color green = new Color(22, 190, 1, 255);
    //private Color red = new Color(190, 45, 1, 255);
    //private Color magenta = new Color(202, 0, 183, 255);
    //private Color blue = new Color(0, 146, 183, 255);

    private int _numReady = 0;
    private bool _gameOver = false;
    private float _restartTimer = 5f;
    private float _restartDelay = 0f;

    private GameState _currentGameState;
    private List<GameObject> _activePlayers;

    private int KokokingSurviveScore = 500; //The score the kokoking gets if the kokoking wins
    private int _pauseSelect = 0; // 0 = Resume, 1 = Restart, 2 = Main Menu
    private bool _pauseSelected = false;
    private GameObject pausePanel;
    private Image resumeImage;
    private Image restartImage;
    private Image mainMenuImage;
    private Text resumeText;
    private Text restartText;
    private Text mainMenuText;
    private Color selectedColor;
    private Color unSelectedColor;
    private Color selectedTextColor;
    private Color unSelectedTextColor;

    private bool _menuConfirm = false;
    private bool _menuYes = false;
    private bool _menuSelected = false;
    private GameObject menuConfirmPanel;
    private Image menuYesImage;
    private Image menuNoImage;
    private Text menuYesText;
    private Text menuNoText;

    private const string _pausePanel = "PausePanel";
    private const string _resumeButton = "Resume";
    private const string _restartButton = "Restart";
    private const string _mainMenu = "MainMenu";
    private const string _countdownTimerName = "Countdown Timer";
    private const string _overtimeMessage = "Overtime Message";

    private const string _menuConfirmPanel = "MenuConfirm";
    private const string _menuYesButton = "ConfirmYes";
    private const string _menuNoButton = "ConfirmNo";

    void Start()
    {
        // we might run into bugs if we initialliaze a variable to a
        // GameSettings field instead of always referencing the static variable.
        // For example, We might want to just use GameSettings.SecondsInRound
        // wherever _secondsInRound is used.
        _secondsInRound = GameSettings.SecondsInRound;
        _numPlayers = GameSettings.numPlayers + 1;
        _numFeathersToWin = GameSettings.FeathersToWin;
        InstantiateFeathersInRound(GameSettings.FeathersInRound);

        _currentGameState = GameState.PreGame;
        SetKokoKing(false);

        _levelMusicVolume = LevelMusic.volume;
        _levelMusicFastVolume = 0f;

        pausePanel = GameUIAnimator.transform.Find(_pausePanel).gameObject;
        resumeImage = pausePanel.transform.Find(_resumeButton).GetComponent<Image>();
        restartImage = pausePanel.transform.Find(_restartButton).GetComponent<Image>();
        mainMenuImage = pausePanel.transform.Find(_mainMenu).GetComponent<Image>();

        selectedColor = resumeImage.color;
        unSelectedColor = restartImage.color;

        resumeText = pausePanel.transform.Find(_resumeButton).GetComponentInChildren<Text>();
        restartText = pausePanel.transform.Find(_restartButton).GetComponentInChildren<Text>();
        mainMenuText = pausePanel.transform.Find(_mainMenu).GetComponentInChildren<Text>();

        selectedTextColor = resumeText.color;
        unSelectedTextColor = restartText.color;

        menuConfirmPanel = GameUIAnimator.transform.Find(_menuConfirmPanel).gameObject;
        menuYesImage = menuConfirmPanel.transform.Find(_menuYesButton).GetComponent<Image>();
        menuNoImage = menuConfirmPanel.transform.Find(_menuNoButton).GetComponent<Image>();
        menuYesText = menuConfirmPanel.transform.Find(_menuYesButton).GetComponentInChildren<Text>();
        menuNoText = menuConfirmPanel.transform.Find(_menuNoButton).GetComponentInChildren<Text>();
    }

    private void InstantiateFeathersInRound(int feathersInRound)
    {
        var featherParent = GameObject.Find("Feathers");
        for (var i = 0; i < feathersInRound; i++)
        {
            var feather = Instantiate(Feather);
            feather.transform.parent = featherParent.transform;
        }
    }

    public bool IsGameOver()
    {
        return _gameOver;
    }

    void SetKokoKing(bool setting)
    {
        RockSpawn1.SetActive(setting);
        RockSpawn2.SetActive(setting);
        leftHand.GetComponent<FireBallSpawner>().enabled = setting;
        rightHand.GetComponent<FireBallSpawner>().enabled = setting;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_currentGameState)
        {
            case GameState.PreGame:
                {
                    // TODO: after a player picks the number of players, each player should click X to play
                    PreGameCutScene.SetActive(true);
                    _cutSceneTimer += Time.deltaTime;

                    if (Input.GetButtonDown("Submit"))
                    {
                        KokoKing.SetActive(true);
                        PreGameCutScene.SetActive(false);
                        _timeLeftInRound = _secondsInRound;
                        _currentGameState = GameState.PreRound;
                    }

                    if (_cutSceneTimer > 15f)
                    {
                        KokoKing.SetActive(true);
                        PreGameCutScene.SetActive(false);
                        HideVRController();
                        _timeLeftInRound = _secondsInRound;
                        _currentGameState = GameState.PreRound;
                    }
                    _activePlayers = players.Where(player => player.activeInHierarchy).ToList();
                    _activePlayers.ForEach(player =>
                    {
                        player.transform.rotation = Quaternion.identity;
                        // TODO: might want to change KokonutProperties to:
                        //     Kokonut.Disable(player);
                        KokonutProperties(player, false);
                        KokonutStop(player);

                        // Disable motion after being enabled so they load in the proper orientation
                        if (_cutSceneTimer > 1f)
                        {
                            player.GetComponent<Motion>().enabled = false;
                        }
                    });
                    return;
                }
            case GameState.PreRound:
                {
                    _startGameTimer -= Time.deltaTime;

                    UpdatePreRoundText();

                    var preRoundTimerOver = _startGameTimer <= _startGameDelay;
                    if (preRoundTimerOver)
                    {
                        _currentGameState = GameState.Round;
                        SetKokoKing(true);
                        HideVRController();
                        _activePlayers.ForEach(player =>
                        {
                            KokonutProperties(player, true);
                        });
                    }

                    _activePlayers.ForEach(player =>
                    {
                        player.transform.rotation = Quaternion.identity;
                    });

                    return;
                }

            case GameState.Round:
                {
                    CheckHelpers();
                    // Pause the game
                    if (Input.GetButtonDown("Submit"))
                    {
                        _currentGameState = GameState.Paused;
                    }

                    _timeLeftInRound -= Time.deltaTime;
                    UpdateRoundTimerUi();

                    // Fast level music starts playing when less than 30 seconds remain
                    if (_timeLeftInRound < 30)
                    {
                        LevelMusicTransition();
                        GameUIAnimator.SetTrigger("TimeLow");
                        VRTimerCanvas.SetTrigger("TimeLow");
                        TimerRHand.GetComponent<Animator>().SetTrigger("TimeLow");
                    }
                    // Or fast level music starts when one Kokonut reaches 15 feathers
                    var leader = GetWinningKokonuts();
                    int LeadingNumFeathers = 0;
                    if (leader.Length > 0)
                    {
                        LeadingNumFeathers = leader[0].GetNumFeathers();
                    }
                    if (LeadingNumFeathers >= _numFeathersToWin - 5)
                    {
                        LevelMusicTransition();
                    }

                    // Update KokoKing Score
                    KokoKingScore = ScoreManager.GetKokoKingScore();
                    leftHand.transform.Find("LeftHandCanvas/Score").gameObject.GetComponent<Text>().text = "Score:" + Environment.NewLine + KokoKingScore;

                    // Set first place Kokonut to have the crown
                    int players_in_lead = 0;
                    _activePlayers.ForEach(player =>
                    {
                        if (LeadingNumFeathers > 0)
                        {
                            if (player.GetComponent<Kokonut>().GetNumFeathers() == LeadingNumFeathers)
                            {
                                players_in_lead++;
                                player.transform.Find("Crown").gameObject.SetActive(true);
                                player.transform.Find("VRCrown").gameObject.SetActive(true);
                            }
                            else
                            {
                                player.transform.Find("Crown").gameObject.SetActive(false);
                                player.transform.Find("VRCrown").gameObject.SetActive(false);
                            }
                        }
                    });

                    // Set crown color depending on place
                    _activePlayers.ForEach(player =>
                    {
                        if (LeadingNumFeathers > 0)
                        {
                            if (players_in_lead > 1)
                            {
                                player.transform.Find("Crown").Find("node_id3").gameObject.GetComponent<Renderer>().material = _grayMat;
                                player.transform.Find("VRCrown").Find("node_id3").gameObject.GetComponent<Renderer>().material = _grayMat;
                            }
                            else
                            {
                                player.transform.Find("Crown").Find("node_id3").gameObject.GetComponent<Renderer>().material = _yellowMat;
                                player.transform.Find("VRCrown").Find("node_id3").gameObject.GetComponent<Renderer>().material = _yellowMat;
                            }
                        }
                    });

                    if (_timeLeftInRound <= 0)
                    {
                        var winners = GetWinningKokonuts();

                        if (winners.Length == 0) return;

                        if (winners[0].GetNumFeathers() < _numFeathersToWin)
                        {
                            KokoKingWin = true;
                        }

                        StopGame();

                        return;
                    }
                    _activePlayers.ForEach((player) =>
                    {
                        var kokonut = player.GetComponent<Kokonut>();
                        var isWinningKokonut = kokonut.GetNumFeathers() >= _numFeathersToWin;
                        if (isWinningKokonut)
                        {
                            _winningKokonut = kokonut;
                            StopGame();
                        }
                    });
                    return;
                }
            case GameState.Paused:
                {
                    Time.timeScale = 0;
                    SetKokoKing(false);
                    _activePlayers.ForEach(player =>
                    {
                        KokonutProperties(player, false);
                    });
                    VRUIAnimator.transform.Find("PausedText").gameObject.SetActive(true);
                    if (!_menuConfirm)
                    {
                        pausePanel.SetActive(true);

                        if (Input.GetAxisRaw("DpadY") == -1 && _pauseSelect < 2 && !_pauseSelected)
                        {
                            _pauseSelected = true;
                            _pauseSelect++;
                        }
                        if (Input.GetAxisRaw("DpadY") == 1 && _pauseSelect > 0 && !_pauseSelected)
                        {
                            _pauseSelected = true;
                            _pauseSelect--;
                        }
                        if (Input.GetAxisRaw("DpadY") == 0)
                        {
                            _pauseSelected = false;
                        }
                    }

                    SetAlpha(_pauseSelect);

                    if (!_menuConfirm)
                    {
                        if (Input.GetButtonDown("Submit") || (_pauseSelect == 0 && Input.GetButton("P0_X")))
                        {
                            Time.timeScale = 1;
                            SetKokoKing(true);
                            _activePlayers.ForEach(player =>
                            {
                                KokonutProperties(player, true);
                            });
                            VRUIAnimator.transform.Find("PausedText").gameObject.SetActive(false);
                            pausePanel.SetActive(false);
                            _currentGameState = GameState.Round;
                        }
                    }

                    if (_pauseSelect == 1)
                    {
                        if (Input.GetButton("P0_X"))
                        {
                            Time.timeScale = 1;
                            Restart();
                        }
                    }
                    if (_pauseSelect == 2)
                    {
                        if (_menuConfirm)
                        {
                            CheckMenuConfirm();
                        }
                        if (Input.GetButtonDown("P0_X") && !menuConfirmPanel.activeSelf)
                        {
                            _menuConfirm = true;
                            pausePanel.SetActive(false);
                            menuConfirmPanel.SetActive(true);
                        }
                        if (!_menuConfirm && _currentGameState == GameState.Paused)
                        {
                            menuConfirmPanel.SetActive(false);
                            pausePanel.SetActive(true);
                        }
                    }
                    return;
                }
            // for now, we'll just treat it the same as PostRound
            case GameState.Overtime:
                {
                    var winners = GetWinningKokonuts();
                    if (winners.Length == 1)
                    {
                        _winningKokonut = winners[0];
                        CleanUpOvertime();
                        StopGame();
                    }
                    return;
                }
            case GameState.PostRound:
                {
                    if (LevelMusicFast.isPlaying)
                    {
                        if (_levelMusicFastVolume > 0f)
                        {
                            _levelMusicFastVolume -= 0.7f * Time.deltaTime;
                            LevelMusicFast.volume = _levelMusicFastVolume;
                        }
                        else
                        {
                            LevelMusic.Stop();
                            LevelMusicFast.Stop();
                            LevelMusic.volume = 0.85f;
                            LevelMusic.Play();
                        }
                    }

                    showKeyboardTimer += Time.deltaTime;
                    if (showKeyboardTimer > showKeyboardDelay)
                    {
                        VRGameOverText.enabled = false;
                        GameOverText.text = "Waiting for Koko King";
                        rightHand.GetComponent<EnterName>().enabled = true;
                        Keyboard.SetActive(true);
                    }

                    if (_DoneShowingKeyboard)
                    {
                        Keyboard.SetActive(false);
                        DisplayLeaderboard(true);
                        rightHand.GetComponent<EnterName>().enabled = false;

                        showLeaderboardTimer += Time.deltaTime;
                        if (showLeaderboardTimer > showLeaderboardDelay)
                        {
                            DisplayLeaderboard(false);
                            _DoneShowingLeaderboard = true;
                            GameSettings.Save();
                        }
                    }

                    if (_DoneShowingKeyboard && _DoneShowingLeaderboard)
                    {
                        _activePlayers.ForEach(player =>
                        {
                            var kokonut = player.GetComponent<Kokonut>();
                            player.transform.Find(kokonut.GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("GameOver");
                        });
                        VRGameOverText.enabled = true;
                        VRGameOverText.text = "Waiting for Kokonuts";
                        GameUIAnimator.SetTrigger("GameOverMenu");
                        GameOverText.text = "Press X to Ready!";
                        if (_menuConfirm)
                        {
                            CheckMenuConfirm();
                        }
                        if (Input.GetButtonDown("Submit") && !menuConfirmPanel.activeSelf)
                        {
                            _menuConfirm = true;
                            menuConfirmPanel.SetActive(true);
                            GameOverText.gameObject.SetActive(false);
                        }
                        if (!_menuConfirm)
                        {
                            menuConfirmPanel.SetActive(false);
                            GameOverText.gameObject.SetActive(true);
                            _activePlayers.ForEach(player => CheckPlayerReadiness(player));
                            if (_numReady == _numPlayers - 1)
                            {
                                _restartTimer -= Time.deltaTime;
                                if (Math.Floor(_restartTimer) >= 0)
                                {
                                    GameOverText.color = Color.white;
                                    GameOverText.text = "Restarting in " + Math.Floor(_restartTimer);
                                }
                                if (_restartTimer <= _restartDelay)
                                {
                                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                                }
                            }
                            else
                            {
                                _restartTimer = 5f;
                            }
                        }
                    }
                    return;
                }

            default:
                // TODO: throw error
                break;
        }

    }
    private IEnumerator ShowJumpHelper(Kokonut kokonut, Motion kokonutMotion)
    {
        kokonut.DisplayJumpHelper();
        yield return new WaitForSeconds(SecondsBetweenHelperRepeat);
        if (kokonutMotion.JumpCount < StopJumpHelperAfterJumpCount)
            yield return StartCoroutine(ShowJumpHelper(kokonut, kokonutMotion));
    }

    private IEnumerator ShowDiveHelper(Kokonut kokonut, Motion kokonutMotion)
    {
        kokonut.DisplayDiveHelper();
        yield return new WaitForSeconds(SecondsBetweenHelperRepeat);
        if (kokonutMotion.DiveCount < StopDiveHelperAfterDiveCount)
            yield return StartCoroutine(ShowDiveHelper(kokonut, kokonutMotion));
    }


    private void CheckHelpers()
    {
        if (!GameSettings.ShowHelpers) return;

        var timePassed = _secondsInRound - _timeLeftInRound;
        var roundLongerThanDiveHelper = _secondsInRound > ShowDiveHelperAfterSeconds;
        if (!_jumpHelperShown && timePassed >= ShowJumpHelperAfterSeconds)
        {
            _jumpHelperShown = true;
            _activePlayers.ForEach(player =>
            {
                var motion = player.GetComponent<Motion>();
                var kokonut = player.GetComponent<Kokonut>();
                if (motion.JumpCount < StopJumpHelperAfterJumpCount)
                {
                    StartCoroutine(ShowJumpHelper(kokonut, motion));
                }
            });
        }

        if (!_diveHelperShown && roundLongerThanDiveHelper && timePassed >= ShowDiveHelperAfterSeconds)
        {
            _diveHelperShown = true;
            _activePlayers.ForEach(player =>
            {
                var motion = player.GetComponent<Motion>();
                var kokonut = player.GetComponent<Kokonut>();
                if (motion.DiveCount < StopDiveHelperAfterDiveCount)
                {
                    StartCoroutine(ShowDiveHelper(kokonut, motion));
                }
            });
        }
    }

    private void CheckMenuConfirm()
    {
        if (Input.GetAxisRaw("DpadY") == 1 && !_menuSelected)
        {
            _menuSelected = true;
            menuYesText.color = selectedTextColor;
            menuNoText.color = unSelectedTextColor;

            menuYesImage.color = selectedColor;
            menuNoImage.color = unSelectedColor;
            _menuYes = true;
        }

        if (Input.GetAxisRaw("DpadY") == -1 && !_menuSelected)
        {
            _menuSelected = true;
            menuYesText.color = unSelectedTextColor;
            menuNoText.color = selectedTextColor;

            menuYesImage.color = unSelectedColor;
            menuNoImage.color = selectedColor;
            _menuYes = false;
        }

        if (Input.GetAxisRaw("DpadY") == 0)
        {
            _menuSelected = false;
        }

        if (Input.GetButtonDown("P0_X"))
        {
            if (_menuYes)
            {
                BackToMain();
            }
            else
            {
                _menuConfirm = false;
            }
        }
    }

    private void LevelMusicTransition()
    {
        if (_levelMusicVolume > 0f)
        {
            _levelMusicVolume -= 0.8f * Time.deltaTime;
            LevelMusic.volume = _levelMusicVolume;
        }
        if (!LevelMusicFast.isPlaying)
        {
            LevelMusicFast.Play();
        }
        if (_levelMusicVolume < 1f)
        {
            _levelMusicFastVolume += 0.8f * Time.deltaTime;
            LevelMusicFast.volume = _levelMusicFastVolume;
        }
    }

    private Kokonut[] GetWinningKokonuts()
    {
        try
        {
            var topScore = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Select(kokonut => kokonut.GetNumFeathers())
                .Max();
            var winners = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Where(kokonut => kokonut.GetNumFeathers() == topScore)
                .ToArray();
            return winners;
        }
        catch
        {
            return new Kokonut[] { };
        }
    }

    private void SetAlpha(int state)
    {
        if (state == 0)
        {
            resumeText.color = selectedTextColor;
            restartText.color = unSelectedTextColor;
            mainMenuText.color = unSelectedTextColor;

            resumeImage.color = selectedColor;
            restartImage.color = unSelectedColor;
            mainMenuImage.color = unSelectedColor;
        }
        if (state == 1)
        {
            resumeImage.color = unSelectedColor;
            restartImage.color = selectedColor;
            mainMenuImage.color = unSelectedColor;

            resumeText.color = unSelectedTextColor;
            restartText.color = selectedTextColor;
            mainMenuText.color = unSelectedTextColor;
        }
        if (state == 2)
        {
            resumeImage.color = unSelectedColor;
            restartImage.color = unSelectedColor;
            mainMenuImage.color = selectedColor;

            resumeText.color = unSelectedTextColor;
            restartText.color = unSelectedTextColor;
            mainMenuText.color = selectedTextColor;
        }
    }

    private void UpdateRoundTimerUi()
    {
        TimeSpan time = TimeSpan.FromSeconds(_timeLeftInRound);
        string timeRemaining = string.Format("{0:D1}:{1:D2}", time.Minutes, time.Seconds);

        UpdateCanvasTimer(GameUIAnimator.transform.Find(_countdownTimerName).gameObject, timeRemaining);
        UpdateCanvasTimer(VRTimerCanvas.transform.Find(_countdownTimerName).gameObject, timeRemaining);
        TimerRHand.GetComponent<Text>().text = "Time:" + Environment.NewLine + timeRemaining;
    }

    private void UpdateCanvasTimer(GameObject canvas, string timeRemaining)
    {
        canvas.SetActive(true);
        canvas.GetComponent<Text>().text = timeRemaining;
    }

    private void UpdatePreRoundText()
    {
        StartGameText.text = Math.Floor(_startGameTimer).ToString("f0");
        VRStartTimerText.text = Math.Floor(_startGameTimer).ToString("f0");

        if (Math.Floor(_startGameTimer) <= 0)
        {
            StartGameText.text = "Start!";
            VRStartTimerText.text = "Start!";
        }

        GameUIAnimator.SetTrigger("StartGame");
        VRUIAnimator.SetTrigger("StartGame");
    }

    private void FlashOvertimeText()
    {
        GameUIAnimator.transform.Find(_countdownTimerName).gameObject.GetComponent<Text>().text = "Overtime! Next feather wins!";
        VRTimerCanvas.transform.Find(_countdownTimerName).gameObject.GetComponent<Text>().text = "Overtime! Next feather wins!";
    }

    private void CleanUpOvertime()
    {
        GameUIAnimator.transform.Find(_overtimeMessage).gameObject.SetActive(false);
    }

    private void CheckPlayerReadiness(GameObject player)
    {
        var kokonut = player.GetComponent<Kokonut>();
        if (_numReady != _numPlayers - 1)
        {
            if (Input.GetButtonDown(kokonut.GetControllerID() + "_X"))
            {
                if (!kokonut.IsReady())
                {
                    kokonut.ReadyUp();
                    player.transform.Find(kokonut.GetID() + " Camera/Canvas/GameOverReady").gameObject.GetComponent<Text>().text = "Ready!";
                }
                else
                {
                    kokonut.NotReady();
                    player.transform.Find(kokonut.GetID() + " Camera/Canvas/GameOverReady").gameObject.GetComponent<Text>().text = "Press X to Ready!";
                }

                if (kokonut.IsReady())
                {
                    _numReady += 1;
                }
                else
                {
                    _numReady -= 1;
                }
            }
        }
    }

    private void StopGame()
    {
        // TODO: I'm not sure that this is the write place for this.
        _currentGameState = GameState.PostRound;
        GameUIAnimator.transform.Find(_countdownTimerName).gameObject.SetActive(false);
        VRTimerCanvas.transform.Find(_countdownTimerName).gameObject.SetActive(false);
        SetKokoKing(false);
        GameObject[] Rocks = GameObject.FindGameObjectsWithTag("Throwable");
        foreach (GameObject rock in Rocks)
        {
            Destroy(rock);
        }
        _activePlayers.ForEach(player =>
        {
            var kokonut = player.GetComponent<Kokonut>();
            kokonut.disabled.volume = 0f;
            player.GetComponentInChildren<CameraScript>().StopCamera();
            player.GetComponentInChildren<CameraScript>().enabled = false;
            KokonutProperties(player, false);
            KokonutStop(player);
        });

        if (!KokoKingWin)
        {
            var winningPlayerId = _winningKokonut.id;

            if (winningPlayerId == 1 || winningPlayerId == 0)
            {
                VRGameOverText.color = Color.green;
                GameOverText.color = Color.green;
            }
            if (winningPlayerId == 2)
            {
                VRGameOverText.color = Color.red;
                GameOverText.color = Color.red;
            }
            if (winningPlayerId == 3)
            {
                VRGameOverText.color = Color.magenta;
                GameOverText.color = Color.magenta;
            }
            if (winningPlayerId == 4)
            {
                VRGameOverText.color = Color.cyan;
                GameOverText.color = Color.cyan;
            }
            GameOverText.text = "Game Over!" + Environment.NewLine + "Player " + winningPlayerId + " Wins!"
                + Environment.NewLine + "Koko King Score: " + KokoKingScore;
            VRGameOverText.text = "Game Over!" + Environment.NewLine + "Player " + winningPlayerId + " Wins!"
                + Environment.NewLine + "Koko King Score: " + KokoKingScore;
        }
        else
        {
            VRGameOverText.color = new Color(1, 1, 0);
            GameOverText.color = new Color(1, 1, 0);
            ScoreManager.UpdateScore(KokokingSurviveScore);
            GameOverText.text = "Koko King Wins!" + Environment.NewLine + "Score: " + KokoKingScore +
                Environment.NewLine + "Bonus " + KokokingSurviveScore + " Points!";
            VRGameOverText.text = "Koko King Wins!" + Environment.NewLine + "Score: " + KokoKingScore +
                Environment.NewLine + "Bonus " + KokokingSurviveScore + " Points!";
        }

        GameUIAnimator.SetBool("GameOver", true);
        VRUIAnimator.SetTrigger("GameOver");
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void BackToMain()
    {
        Time.timeScale = 1;
        Destroy(GameObject.Find("GameSettings"));
        SceneManager.LoadScene(0);
    }

    private void KokonutProperties(GameObject player, bool setting)
    {
        player.GetComponent<Motion>().enabled = setting;
        player.GetComponent<Kokonut>().enabled = setting;
        player.GetComponent<CollectFeather>().enabled = setting;
        player.GetComponent<DisableByHit>().enabled = setting;
        player.GetComponent<WindGustSpawn>().enabled = setting;
    }

    private void KokonutStop(GameObject kokonut)
    {
        kokonut.GetComponent<Rigidbody>().velocity = Vector3.zero;
        kokonut.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void DoneKeyboard(bool setting)
    {
        _DoneShowingKeyboard = setting;
    }

    private void DisplayLeaderboard(bool setting)
    {
        foreach (GameObject board in Leaderboards)
        {
            board.SetActive(setting);
            GameSettings.UpdateLeaderboardDisplay(board);
        }
    }

    private void HideVRController()
    {
        if (leftHand != null)
        {
            try
            {
                GameObject handL = leftHand.transform.Find("LeftRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handL != null)
                {
                    handL.SetActive(false);
                }
            }
            catch
            {
                print("Left hand not found");
            }
        }
        if (rightHand != null)
        {
            try
            {
                GameObject handR = rightHand.transform.Find("RightRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handR != null)
                {
                    handR.SetActive(false);
                }
            }
            catch
            {
                print("Right hand not found");
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManager : MonoBehaviour {

    public GameObject MagnetBuff;
    public GameObject SpeedBuff;
    public GameManager manager;

    private float timer = 0f;
    private float delay = 20f;

    public int MaxBuffsInPlay = 2;
    private int numBuffsInPlay = 0;

    private List<GameObject> buffList;

    private int[] x_range = { -15, 90 };
    private int[] y_range = { 16, 50 };
    private int[] z_range = { 120, 170 };

    private System.Random rnd = new System.Random();

    // Use this for initialization
    void Start () {
        rnd = new System.Random(System.DateTime.Now.Millisecond);
        buffList = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!manager.IsGameOver())
        {
            for (var i = buffList.Count - 1; i > -1; i--)
            {
                if (buffList[i] == null)
                {
                    buffList.RemoveAt(i);
                }
            }
            numBuffsInPlay = buffList.Count;
            if (numBuffsInPlay < MaxBuffsInPlay)
            {
                timer += Time.deltaTime;
                if (timer >= delay)
                {
                    int positionx = Random.Range(x_range[0], x_range[1]);
                    int positiony = Random.Range(y_range[0], y_range[1]);
                    int positionz = Random.Range(z_range[0], z_range[1]);
                    rnd = new System.Random(System.DateTime.Now.Millisecond);

                    int pickBuff = rnd.Next(0, 10);
                    if (pickBuff < 5)
                    {
                        GameObject buff = Instantiate(MagnetBuff, new Vector3(positionx, positiony, positionz), MagnetBuff.transform.rotation);
                        buffList.Add(buff);
                    }
                    else
                    {
                        GameObject buff = Instantiate(SpeedBuff, new Vector3(positionx, positiony, positionz), SpeedBuff.transform.rotation);
                        buffList.Add(buff);
                    }
                    timer = 0f;
                }
            }
        }
	}
}

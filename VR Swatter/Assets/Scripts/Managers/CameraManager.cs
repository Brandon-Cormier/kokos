﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public GameObject[] players;
    private int numPlayers;

    public Transform TimeTransform;

    private Camera cam;

	// Use this for initialization
	void Start () {
        numPlayers = GameSettings.numPlayers;

        if (numPlayers == 1)
        {
            players[0].SetActive(true);
            TimeTransform.localPosition = new Vector3(120, 175, 0);
        }

        if (numPlayers == 2)
        {
            players[1].SetActive(true);
            players[2].SetActive(true);
            TimeTransform.localPosition = new Vector3(120, 175, 0);

            cam = players[1].GetComponentInChildren<Camera>();
            cam.rect = new Rect(new Vector2(0, 0), new Vector2(0.5f, 1));

            cam = players[2].GetComponentInChildren<Camera>();
            cam.rect = new Rect(new Vector2(0.5f, 0), new Vector2(0.5f, 1));
        }

        if (numPlayers == 3)
        {
            players[1].SetActive(true);
            players[2].SetActive(true);
            players[3].SetActive(true);

            SetCameraTopLeft(1);
            SetCameraTopRight(2);

            cam = players[3].GetComponentInChildren<Camera>();
            cam.rect = new Rect(new Vector2(0, 0), new Vector2(1, 0.5f));
        }

        if (numPlayers == 4)
        {
            players[1].SetActive(true);
            players[2].SetActive(true);
            players[3].SetActive(true);
            players[4].SetActive(true);

            SetCameraTopLeft(1);
            SetCameraTopRight(2);
            SetCameraBottomLeft(3);
            SetCameraBottomRight(4);
        }
    }

    private void SetCameraTopLeft(int player)
    {
        cam = players[player].GetComponentInChildren<Camera>();
        cam.rect = new Rect(new Vector2(0, 0.5f), new Vector2(0.5f, 0.5f));
    }

    private void SetCameraTopRight(int player)
    {
        cam = players[player].GetComponentInChildren<Camera>();
        cam.rect = new Rect(new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f));
    }

    private void SetCameraBottomLeft(int player)
    {
        cam = players[player].GetComponentInChildren<Camera>();
        cam.rect = new Rect(new Vector2(0, 0), new Vector2(0.5f, 0.5f));
    }

    private void SetCameraBottomRight(int player)
    {
        cam = players[player].GetComponentInChildren<Camera>();
        cam.rect = new Rect(new Vector2(0.5f, 0), new Vector2(0.5f, 0.5f));
    }

    // Update is called once per frame
    void Update () {

	}
}
